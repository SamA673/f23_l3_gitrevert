package dawson;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void shouldAnswerWithX()
    {
        assertEquals("Testing if 5", 5, App.echo(5) );
    }

    @Test
    public void shouldAnswerWithXPlusOne(){
        assertEquals("Should return x + 1 (6)", 6, App.oneMore(5));
    }
    


}
